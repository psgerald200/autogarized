import os       # lib for access operating system
import shutil   # lib for moving file
import spacy    # lib for nlp funct

nlp = spacy.load('en_core_web_md') # Spacy CNN Models 

# Path
srcpath = "D:\\Test"
srcfiles = os.listdir(srcpath)

destpath = "D:\\Test\\Mathematics"
destpath_name = 'Mathematics'

# Remove STRING that contains the path and extension
def get_filename_without_extension(file_path):
    file_basename = os.path.basename(file_path)
    filename_without_extension = file_basename.split('.')[0]
    return filename_without_extension


files = []

# r=root, d=directories, f = files
for r, d, f in os.walk(srcpath):
    for file in f:
        files.append(os.path.join(r, file))


for f in files:
    word = get_filename_without_extension(f) 
    print(word)
    word1 = nlp(word)           # Checking the vocab of the file name
    word2 = nlp(destpath_name)  # Checking the vocab of the dir name
    simi = word1.similarity(word2) # Calculating the Vector Value of semantics between the filename and dirname
    print(simi) 
    if simi >= 0.5: # the treshold that we used is 0.5
        shutil.move(os.path.join(srcpath, f), destpath) # moving file from src directory to the destination using shutil